﻿using System;
using System.Collections;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif

using UnityEngine;

namespace UnityEngine.Tilemaps
{
	[Serializable]
	public class RandomDensityTile : Tile
	{
        [SerializeField] public Sprite emptySprite;

        [SerializeField] public Sprite[] sprites;

        [SerializeField] public float probability = 0.5f;

        public override void GetTileData(Vector3Int location, ITilemap tileMap, ref TileData tileData)
		{
			base.GetTileData(location, tileMap, ref tileData);
			if ((sprites != null) && (sprites.Length > 0))
			{
                long hash = location.x;
				hash = (hash + 0xabcd1234) + (hash << 15);
				hash = (hash + 0x0987efab) ^ (hash >> 11);
				hash ^= location.y;
				hash = (hash + 0x46ac12fd) + (hash << 7);
				hash = (hash + 0xbe9730af) ^ (hash << 11);

                Random.InitState((int)hash);
                
			    if (Random.value > probability)
			        tileData.sprite = emptySprite;
			    else
                    tileData.sprite = sprites[(int)(sprites.Length * Random.value)];
            }
		}

#if UNITY_EDITOR
        [MenuItem("Assets/Create/Custom Tiles/Random Density Tile")]
        public static void CreateRandomTile()
        {
            AssetDatabase.CreateAsset(ScriptableObject.CreateInstance<RandomDensityTile>(), "Assets/Tilemap/Tiles/New RandomDensityTile.asset");
        }
#endif
    }

#if UNITY_EDITOR
	[CustomEditor(typeof(RandomDensityTile))]
	public class RandomDensityTileEditor : Editor
	{
		private RandomDensityTile tile { get { return (target as RandomDensityTile); } }

		public override void OnInspectorGUI()
		{
			EditorGUI.BeginChangeCheck();

            tile.probability = EditorGUILayout.Slider("Density", tile.probability, 0, 1);

            tile.emptySprite = (Sprite) EditorGUILayout.ObjectField("Main Sprite", tile.emptySprite, typeof(Sprite), false, null);
            EditorGUILayout.Space();
			
            int count = EditorGUILayout.DelayedIntField("Number of Variants", tile.sprites != null ? tile.sprites.Length : 0);

			if (count < 0)
				count = 0;

			if (tile.sprites == null || tile.sprites.Length != count)
				Array.Resize<Sprite>(ref tile.sprites, count);

			if (count == 0)
				return;

            for (int i = 0; i < count; i++)
			    tile.sprites[i] = (Sprite) EditorGUILayout.ObjectField("Sprite " + (i+1), tile.sprites[i], typeof(Sprite), false, null);
            
			if (EditorGUI.EndChangeCheck())
				EditorUtility.SetDirty(tile);
		}
	}
#endif
}
