﻿using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class IngredientSlot : MonoBehaviour
{
    [SerializeField] private Image icon;

    [SerializeField] private GameObject amountPanel;

    [SerializeField] private Text amountText;

    [SerializeField] public Ingredient ingredient;

    private int amount = 0;

    public bool discovered = false, newly = false;

    public void Start()
    {
        icon.sprite = discovered ? ingredient.sprite : ingredient.shadow;
    }

    public void add()
    {
        newly = newly || !discovered;
        discovered = true;
        icon.sprite = ingredient.sprite;
        amountPanel.SetActive(true);

        amount++;
        amountText.text = amount.ToString();
    }

    public void slotPressed()
    {
        Debug.Log("Pressed " + ingredient.name + " slot");
    }
}
