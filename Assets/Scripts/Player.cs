﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{
    [SerializeField]
    private Genders gender;
    private Genders lastGender;

    private enum Genders
    {
        Female, Male
    }

    [SerializeField] private float moveSpeed;
    [SerializeField] [Range(0,1)] private float diagonalSpeed;

    private new Rigidbody2D rigidbody;

    private Vector2 direction;

    private Animator animator;

    void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        float HInput = Input.GetAxisRaw("Horizontal");
        float VInput = Input.GetAxisRaw("Vertical");

        /* Movements */
        float currentSpeed = HInput != 0 && VInput != 0 ? moveSpeed*diagonalSpeed : moveSpeed;
        rigidbody.velocity = new Vector2(HInput * currentSpeed, VInput * currentSpeed);

        if (HInput != 0 || VInput != 0)
            direction = new Vector2(HInput, VInput);

        /* Animations */
        animator.SetFloat("Input X", HInput);
        animator.SetFloat("Input Y", VInput);
        animator.SetFloat("Direction X", direction.x);
        animator.SetFloat("Direction Y", direction.y);
        animator.SetBool("Is Moving", HInput != 0 || VInput != 0);
        
        /* Gender Swap */
        if (gender != lastGender)
        {
            animator.SetTrigger("Gender Swap");
            lastGender = gender;
        }
    }
}