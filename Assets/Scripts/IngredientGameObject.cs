﻿using UnityEngine;

public class IngredientGameObject : MonoBehaviour
{
    [SerializeField] private Ingredient ingredient;

    void OnTriggerEnter2D(Collider2D other)
    {
        Inventory.add(ingredient);
        Destroy(this.gameObject);
    }

    void OnValidate()
    {
        GetComponent<SpriteRenderer>().sprite = ingredient.sprite;
    }
}