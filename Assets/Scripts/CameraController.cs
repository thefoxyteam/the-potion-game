﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private Transform target;

    //[SerializeField] private float moveSpeed;    

	void Update ()
    {
		transform.position = new Vector3(target.position.x,
                                         target.position.y,
                                         this.transform.position.z);

        //transform.position = Vector3.Lerp(transform.position, target, moveSpeed * Time.deltaTime);
    }
}
