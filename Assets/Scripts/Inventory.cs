﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Inventory : MonoBehaviour
{
    [SerializeField] private GameObject inventory;

    private static Dictionary<Ingredient, IngredientSlot> slots;
    private static SortedDictionary<Ingredient, IngredientSlot> sortedSlots;

    void Start()
    {
        IngredientSlot[] array = GetComponentsInChildren<IngredientSlot>(true);
        slots = array.ToDictionary(key => key.ingredient, value => value);
        sort();
    }

    void Update()
    {
        if (Input.GetButtonDown("Ingredients"))
            toggleInventory();
    }

    public void toggleInventory()
    {
        inventory.SetActive(!inventory.activeSelf);
    }

    public static void add(Ingredient ingredient)
    {
        slots[ingredient].add();
        sort();
    }

    private static void sort()
    {
        sortedSlots = new SortedDictionary<Ingredient, IngredientSlot>(slots, new IngredientSlotComparer());
        foreach (IngredientSlot i in sortedSlots.Values)
            i.transform.SetAsLastSibling();
    }

    public class IngredientSlotComparer : IComparer<Ingredient>
    {
        public int Compare(Ingredient x, Ingredient y)
        {
            if (slots[x].discovered && !slots[y].discovered)
                return -1;

            if (!slots[x].discovered && slots[y].discovered)
                return 1;

            return string.Compare(x.name, y.name);
        }
    }
}