﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Ingredient", menuName = "Potion Game/Ingredient")]
public class Ingredient : ScriptableObject
{
    public new string name = "Ingredient Name";

    public Sprite sprite, shadow;
}
